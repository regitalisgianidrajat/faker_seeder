@extends('template.main')
@section('content')
<style>
.btn-light{
    padding:15px;
}
</style>
    <h1 class="mt-4 mb-4" style="margin-bottom:0px!important">{{$title}}</h1>
    
    <ol class="breadcrumb" style="background-color:#fff">
          <li class="breadcrumb-item"><a href="{{ url('/user/') }}">Home</a></li>
          <li class="breadcrumb-item active">{{ $title }}</li>
        </ol>
    <hr>
    @if($errors->any())
        <div class="card-body notif-message">
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{$errors->first()}}
            </div>
        </div>
    @endif
    <form action="{{ url('/update',$data['user_id']) }}"method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">First Name</label>
            <input class="form-control" type="text" name="firstname" id="name" value="{{ $data['firstname'] }}" placeholder="First Name" required>
        </div>
        <div class="form-group">
            <label for="name">Last Name</label>
            <input class="form-control" type="text" name="lastname" id="name" value="{{ $data['lastname'] }}" placeholder="Last Name" required>
        </div>
        <div class="form-group">
            <label for="type">Email</label>
            <input class="form-control" type="email" name="email" id="email" value="{{ $data['email'] }}" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="type">Company</label>
            <select name="company_id" id="company_id" class="form-control" required>
                <option value="">--- Select Company ---</option>
                @foreach ($company as $key)
                    <option value="{{ $key['company_id'] }}" @if($data['company_id'] == $key['company_id']) {{'selected'}} @endif>{{  $key['company_name'] }}</option>
                @endforeach
            </select>
        </div>
        
        <div class="form-group">
            <label for="type">Event</label>
            <select multiple name="event[]" id="event_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Event ---</option>
                @foreach ($event as $key)
                    <option
                        @foreach($data['event'] as $raw)
                            @if($raw['event_id'] == $key['event_id'])
                                {{'selected'}}
                            @endif
                        @endforeach
                    value="{{$key['event_id']}}">{{$key['event_name']}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group float-right">
            <button class="btn btn-lg btn-danger" type="reset">Reset</button>
            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>
        
    </form>

@endsection