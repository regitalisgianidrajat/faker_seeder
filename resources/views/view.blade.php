@extends('template.main')
@section('content')
    <h1 class="mt-4 mb-4">{{$title}}
    <a class="btn btn-primary float-right mt-2" href="{{url('/add')}}" role="button">Create User</a></h2><hr>
    @if(Session::get('alert-success'))
        <div class="card-body notif-message">
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{ Session::get('alert-success') }}
        </div>
        </div>
    @endif
    <table id="data_users_reguler" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Company</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                    <td>{{ $row->firstname.' '.$row->lastname }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->company->company_name}}</td>
                    <td>
                        <a href="{{ url('/edit/'.$row["user_id"])}}" class="btn btn-xs btn-primary">Edit</a> |
                        <a href="{{ url('/delete/'.$row["user_id"])}}"" class="btn btn-xs btn-danger" onclick="return confirm('Are you want to delete this data ?');">Delete</a>
                    </td>
                </tr>
            @endforeach
            
    </table>
@endsection