<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model
{
    use SoftDeletes;

    protected $table   = 'users';
	public $primarykey = 'user_id';
    public $timestamps = true;
    protected $fillable = [
		'company_id',
		'firstname',
		'lastname',
		'email'
	];
	protected $casts = [
		'company_id'=> 'integer',
		'firstname' => 'string',
		'lastname' 	=> 'string',
		'email' 	=> 'string'
	];
		
	protected $hidden = [
		'status',
		'created_at',
		'updated_at',
		'deleted_at'
    ];
    public function company()
    {
    	return $this->hasOne('App\Models\CompanyModel','company_id', 'company_id');
    }
    public function event()
    {
        return $this->hasMany('App\Models\EventUserModel','user_id', 'user_id');
    }
}
