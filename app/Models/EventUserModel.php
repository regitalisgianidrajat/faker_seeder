<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventUserModel extends Model
{
    use SoftDeletes;

    protected $table   = 'user_event';
	public $primarykey = 'user_event_id';
    public $timestamps = true;
    protected $fillable = [
		'user_id',
		'event_id'
	];
	protected $casts = [
		'user_id' 	=> 'integer',
		'event_id' 	=> 'integer'
	];
		
	protected $hidden = [
        'user_event_id',
		'created_at',
		'updated_at',
		'deleted_at'
    ];

    public function events()
    {
        return $this->belongsTo('App\Models\EventModel', 'user_event_id', 'event_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\UserModel', 'user_event_id', 'user_id');
    }
}
