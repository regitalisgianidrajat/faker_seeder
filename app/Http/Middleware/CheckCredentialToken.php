<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;

class CheckCredentialToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'STATUS' => 401,
                'MESSAGE' => 'Token not provided.',
                'DATA' => (object) array()
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'STATUS' => 402,
                'MESSAGE' => 'Provided token is expired.',
                'DATA' => (object) array()
            ], 402);
        } catch(Exception $e) {
            return response()->json([
                'STATUS' => (method_exists($e, 'getStatusCode')) ? $e->getStatusCode() : 500,
                'MESSAGE' => 'Auth - '.$e->getMessage().' - '.$e->getFile().' - L '.$e->getLine(),
                'DATA' => (object) array(),
            ],(method_exists($e, 'getStatusCode')) ? $e->getStatusCode() : 500);
        }
        return $next($request);
    }
}
