<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserModel;
use App\Models\CompanyModel;
use App\Models\EventModel;
use App\Models\EventUserModel;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    private $user;
    private $company;
    private $event;

    public function __construct()
    {
        $this->user     = UserModel::select('*');
        $this->company  = CompanyModel::select('*');
        $this->event    = EventModel::select('*');
    }
    public function GetUser()
    {
        $data  = $this->user->with('company')->with('event')->get();
        if (!$data->isEmpty()) {
            
            $collect =  collect($this->event->get());
            $data= $data->map(function($key) use($collect){
                $key['event']= collect($key['event'])->map(function($event)use($collect){
                    $event['event_name']  = $collect->where('event_id', $event['event_id'])->pluck('event_name')->first();
                    $event['event_code']  = $collect->where('event_id', $event['event_id'])->pluck('event_code')->first();
                    return $event;
                });
                return $key;
            });
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', array());
        }
        
    }
    public function DetailUser(Request $request)
    {
        $role = [
            'user_id' => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', $ErrorMsg, new \stdClass());
        }
        $data  = $this->user->where('user_id',$request['user_id'])->with('company')->with('event')->get();

        if (!empty($data)) {
            $collect =  collect($this->event->get());
            $data= $data->map(function($key) use($collect){
                $key['event']= collect($key['event'])->map(function($event)use($collect){
                    $event['event_name']  = $collect->where('event_id', $event['event_id'])->pluck('event_name')->first();
                    $event['event_code']  = $collect->where('event_id', $event['event_id'])->pluck('event_code')->first();
                    return $event;
                });
                return $key;
            })[0];
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', new \stdClass());
        }
        
    }

    public function CreateUser(Request $request)
    {
        $PostRequest = $request->only('company_id','firstname','lastname','email');
        $role = [
            'company_id' => 'Required',
            'firstname' => 'Required',
            'lastname'  => 'Required',
            'email'     => 'Required|email|unique:App\Models\UserModel,email',
            "event"     => "required|array|min:0"
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', $ErrorMsg, new \stdClass());
        }
        
        $saved = UserModel::create($PostRequest);

        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT SAVED', new \stdClass());
        }
        $Post['user_id'] = $saved->id;
        for ($i=0; $i < count($request['event']) ; $i++) { 
            $Post['event_id'] = $request['event'][$i];
            EventUserModel::create($Post);
        }
        
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
    }

    public function GetCompany()
    {
        $data  = $this->company->get();
        if (!$data->isEmpty()) {
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', array());
        }
        
    }
    public function GetEvent()
    {
        $data  = $this->event->get();
        if (!$data->isEmpty()) {
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', array());
        }
        
    }
}
