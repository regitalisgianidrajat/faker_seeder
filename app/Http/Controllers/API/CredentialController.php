<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CredentialModel;
use Illuminate\Http\Request;

class CredentialController extends Controller
{
    private $credential;

    public function __construct()
    {
        $this->credential    = CredentialModel::select('*');
    }
    public function Create(Request $request)
    {
        $PostRequest = $request->only(
            'user',
            'secret'
        );

        $role = [
            'user' => 'Required',
            'secret' => 'Required'
        ];
        $ErrorMsg = $this->Validator($PostRequest, $role);

        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus(400, 'Failed Validator Error, Please Complete All Data! ' . $ErrorMsg, new \stdClass());
        }
        $check = $this->credential->select('*')->where('user','=',$request['user'])->where('secret','=',$request['secret'])->first();
        if (!empty($check)) {
            return $this->jwt($check);
        }else{
            return $this->ResponseStatus(404, 'Failed! Credential not found or Inactived!',(object)array());
        }
        
    }

}
