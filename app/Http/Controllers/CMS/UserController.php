<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\UserModel;
use App\Models\CompanyModel;
use App\Models\EventModel;
use App\Models\EventUserModel;
use Illuminate\Http\Request;
use Redirect;

class UserController extends Controller
{
    private $user;
    private $company;
    private $event;

    public function __construct()
    {
        $this->user     = UserModel::select('*');
        $this->company  = CompanyModel::select('*');
        $this->event    = EventModel::select('*');
    }
    public function index()
    {
        $data['data']  = $this->user->with('company')->get();
        $data['title'] = 'Manage User';
    	return view('view', $data);
        
    }
    public function create(){
        $data['company']  = $this->company->get();
        $data['event']    = $this->event->get();
		$data['title']    = 'Create User';
    	return view('add', $data);
    }

    public function store(Request $request)
    {
        $PostRequest = $request->only('company_id','firstname','lastname','email');
        $role = [
            'company_id' => 'Required',
            'firstname' => 'Required',
            'lastname'  => 'Required',
            'email'     => 'Required|email|unique:App\Models\UserModel,email',
            "event"     => "required|array|min:0"
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }
        
        $saved = UserModel::create($PostRequest);

        if(!$saved){
            return Redirect::back()->withErrors(['Server Error !']);
        }
        $Post['user_id'] = $saved->id;
        for ($i=0; $i < count($request['event']) ; $i++) { 
            $Post['event_id'] = $request['event'][$i];
            EventUserModel::create($Post);
        }
        
		return redirect('/user')->with('alert-success','User Successfully Created !');
    }

    public function show($id)
    {
        $data['data']     = $this->user->where('user_id',$id)->with('company')->with('event')->first();
        $data['company']  = $this->company->get();
        $data['event']    = $this->event->get();
        $data['title']    = 'Edit User';
    	return view('edit', $data);
    }

    public function update(Request $request, $id)
    {
        $UpdateRequest = $request->only('company_id','firstname','lastname','email');
        $role = [
            'company_id' => 'Required',
            'firstname' => 'Required',
            'lastname'  => 'Required',
            'email'     => 'Required|email',
            "event"     => "required|array|min:0"
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }

        $update = $this->user->where('user_id',$id)->update($UpdateRequest);

        if(!$update){
            return Redirect::back()->withErrors(['Server Error !']);
        }
        EventUserModel::onlyTrashed()->where('user_id',$id)->forceDelete();
        $Post['user_id'] = $id;
        for ($i=0; $i < count($request['event']) ; $i++) { 
            $Post['event_id'] = $request['event'][$i];
            EventUserModel::create($Post);
        }
		return redirect('/user')->with('alert-success','User Successfully Updated !');
    }

    public function destroy($id)
    {
        $saved = $this->user->where('user_id',$id)->delete();
        if(!$saved){
            return Redirect::back()->withErrors(['Server Error !']);
        }
        
		return redirect('/user')->with('alert-success','User Successfully Deleted !');
        
    }
}
