<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        \App\Models\UserModel::create([
            'company_id'=> 1,
            'firstname' => 'User',
            'lastname'  => 'Default',
            'email'     => 'userdefault@mailinator.com'
        ],);
    }
}
