<?php

use Illuminate\Database\Seeder;

class CredentialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CredentialModel::create([
            'name'    => 'faker&seeder',
            'user'    => 'faker&seeder',
            'secret'  => 'faker&seeder'
        ],);
    }
}
